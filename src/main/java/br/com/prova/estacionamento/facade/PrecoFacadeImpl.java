package br.com.prova.estacionamento.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.prova.estacionamento.Resposta;
import br.com.prova.estacionamento.entity.Preco;
import br.com.prova.estacionamento.repository.PrecoRepository;

@Stateless
public class PrecoFacadeImpl implements PrecoFacade{

	@EJB
	private PrecoRepository precoRepository;
	
	@Override
	public Resposta buscaPreco(String marca, String modelo, String placa) {
		 Resposta resposta = new Resposta();
		 resposta.setDescricao("Valor devido e de R$ ");
		 resposta.setValor("5.00");
		 if(placa.toUpperCase().contains("B") && placa.endsWith("99")){
			 resposta.setValor("1.00");
			 return resposta;
		 }
		 if(marca.toUpperCase().equals("FORD")){
			 resposta.setValor("10.00");
			 return resposta;
		 }
		 Preco preco = precoRepository.buscaVeiculo(marca, modelo, placa);
		 if(preco != null){
			 resposta.setValor(preco.getPreco().toString());
		 }
		return resposta;
	}

	
}
