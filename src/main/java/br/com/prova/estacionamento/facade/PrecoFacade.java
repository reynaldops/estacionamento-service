package br.com.prova.estacionamento.facade;

import javax.ejb.Local;

import br.com.prova.estacionamento.Resposta;

@Local
public interface PrecoFacade {

	Resposta buscaPreco(String marca, String modelo, String placa);
	
}
