package br.com.prova.estacionamento.repository;

import javax.ejb.Local;

import br.com.prova.estacionamento.entity.Preco;


@Local
public interface PrecoRepository {

	Preco buscaVeiculo(String marca, String modelo, String placa);
}
