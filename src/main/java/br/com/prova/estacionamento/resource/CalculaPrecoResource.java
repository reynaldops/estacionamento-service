package br.com.prova.estacionamento.resource;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.prova.estacionamento.Resposta;
import br.com.prova.estacionamento.entity.Preco;
import br.com.prova.estacionamento.facade.PrecoFacade;

@Path("/")
public class CalculaPrecoResource {
	
	@EJB
	private PrecoFacade precoFacade;
	
	@GET
	@Path("/buscaPreco")
	public Response buscaPreco(@QueryParam("marca")String marca,
							   @QueryParam("modelo") String modelo,
						       @QueryParam("placa")String placa){
		
		Resposta resposta = precoFacade.buscaPreco(marca, modelo, placa);
		return Response.ok(resposta, MediaType.APPLICATION_JSON).build();
	}
}
